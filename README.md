
## Descrição

Este foi o trabalho final do primeiro ano do curso de informática do Cefet MG - Campus Contagem, codificado em 2020.
O Objetivo era criar uma implementação terminal-based do famoso jogo "Campo Minado".

## Compilando

Se você está no Windows, faça o download do arquivo CampoMinado.exe nas [releases](https://gitlab.com/trueSamukinha/campo-minado-em-c/-/releases/v1.0.0).


Se você está em um sistema operacional Unix Like, siga as instruções abaixo:

- Clone o repositório : `git clone https://gitlab.com/trueSamukinha/campo-minado-em-c.git ~/campo-minado-em-c`
- Navegue até o diretório: `cd ~/campo-minado-em-c`
- Compile o arquivo CampoMinado.c: `gcc -o "campoMinado.out" ./CampoMinado.c`
- Execute o arquivo: `./campoMinado.out`